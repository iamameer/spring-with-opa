package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

	private List<User> users = new ArrayList<>();

	@GetMapping
	public List<User> getAllUsers() {
		return users;
	}

	@PostMapping
	public String createUser(@RequestBody User user) {
		users.add(user);
		return "User created";
	}

}
