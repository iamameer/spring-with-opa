package access

default allow_access = false

#only admin can create user [POST]
allow {
  some username
  input.method == "GET"
  input.user == "ADMIN"
}

#only auth user can read [GET]
