
# Spring with OPA

Basic GET/POST with OPA implementation

### 1) Prerequisites
- 1.1 Java/JDK installed
- 1.2 JAVA_HOME path setup

### 2) Project
2.1 Git clone
```
https://gitlab.com/iamameer/spring-with-opa.git
```
The project is initialized from a [Spring Tempalte](https://gitlab.com/gitlab-org/project-templates/spring)

### 3) Run
Using maven wrapper:
```
./mvnw spring-boot:run
```
### 4) API endpoints
- 4.1 /api/users [GET]
Returning all users

- 4.2 /api/users [POST]
body:
{
    "name": "john_doe",
    "email": "john@example.com"
}
Will create a users

* Note that no database implemented, just a List of users.

### 5) Docker

- 5.1 Setup
Install and run Docker, then create api image
```
docker image  ##-To check any existing Images
docker build -t springapi .
docker image  ##-To verify the created image
docker run -d -p 8000:8080 springapi:latest
```
- 5.2 Testing
Test the endpoint from the new forwarded port:
```
curl http://localhost:8000/api/users
```

* for removal: docker rm -f [imageID]

### 6) OPA!
Pull OPA Image and run it:
```
docker pull openpolicyagent/opa:edge-rootless
docker run -it --rm -p 8181:8181 openpolicyagent/opa run --server --addr :8181
docker container ls ##-To verify the running image
```

### 7) Interact with OPA
- 7.1 Via Curl (bash):
```
curl -X POST -H "Content-Type: application/json" -d '{"query": "data.system.version"}' http://localhost:8181/v1/data

```
- 7.2 Via Powershell :
```
Invoke-WebRequest -Method Post -Uri http://localhost:8181/v1/data -ContentType "application/json" -Body '{"input": {}, "query": "data.system.version"}'
```

### 8) Minikube
- 8.1 Install and run minikube:
```
minikube start
```
- 8.2 Start the pod
```
kubectl create -f ./springkube.yaml
kubectl get pods
```
or error, run: kubectl decribe pod
or ErrPull/Back: (powershell) & minikube docker-env | Invoke-Expression, rebuild the image and kubectl apply yaml


OPA references:
https://www.baeldung.com/spring-security-authorization-opa
https://youtu.be/M7IwpC9WpIg
